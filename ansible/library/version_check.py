#!/usr/bin/python
import yaml
import re
from yamllint import linter
from yamllint.config import YamlLintConfig
from ansible.module_utils.basic import *


def os_change_detect(new_yaml, old_yaml):
	def validate_platforms(dic):
		f = open('../files/supported_platforms', 'r')
		platforms = yaml.load(f, Loader=yaml.FullLoader)
		errored = [x for x in dic.keys() if x not in platforms['Cisco']]
		if errored:
			return False
		else:
			return True

	def sw_bin_file_validation(dic):
		for x in dic.values():
			if re.search('\S.+\.bin$', x):
				continue
			else:
				return False

		return True

	# open yaml files
	new_yaml_file = open(new_yaml, 'r')
	new_yaml_val_file = open(new_yaml, 'r')
	old_yaml_file = open(old_yaml, 'r')

	# lint new yaml file
	conf = YamlLintConfig('extends: default')
	lint_result = linter.run(new_yaml_val_file, conf)
	errors = list(lint_result)
	if errors:
		raise Exception(f'YAML Syntax check failed. Please Fix the following -- {errors}')

	# read yaml files into dictionaries
	new_dic = yaml.load(new_yaml_file, Loader=yaml.FullLoader)
	old_dic = yaml.load(old_yaml_file, Loader=yaml.FullLoader)

	# close files
	new_yaml_file.close()
	new_yaml_val_file.close()
	old_yaml_file.close()

	# input validaiton checks
	platform_result = validate_platforms(new_dic)
	sw_result = sw_bin_file_validation(new_dic)

	if platform_result is False:
		raise Exception("invalid platform entered")
	if sw_result is False:
		raise Exception("invalid SW bin file entered")

	# Start Analyzing for new SW code verison suggestions

	in_common = [x for x in old_dic.keys() if x in new_dic.keys()]

	os_request = []
	for device in in_common:
		if old_dic[device] != new_dic[device]:
			# print(f'Code version change detected. Old version - {old_dic[device]}. Suggested version is {new_dic[device]}')
			os_request.append({'platform': device, 'old_version': old_dic[device], 'new_version': new_dic[device]})
		else:
			os_request.append({'platform': device, 'old_version': old_dic[device], 'new_version': new_dic[device]})

	return os_request

def main():

	module_args = dict(
		master=dict(type='str', required=True),
		proposed=dict(type='str', required=True)
	)

	fields = {
		"master_sw": {"required": True, "type": "str"},
		"proposed_sw": {"required": True, "type": "str"}
	}

	module = AnsibleModule(argument_spec=fields)
	master_sw_versions = module.params['master_sw']
	proposed_sw_versions = module.params['proposed_sw']
	result = os_change_detect(proposed_sw_versions, master_sw_versions)

	if result[0]['old_version'] != result[0]['new_version']:
		module.exit_json(changed=True, meta=result)
	elif result[0]['old_version'] == result[0]['new_version']:
		module.exit_json(changed=False, meta=result)
	else:
		module.fail_json(changed=False, meta=result)

if __name__ == '__main__':
	main()

