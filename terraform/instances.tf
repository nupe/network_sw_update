# Define Instance security group

resource "aws_security_group" "allow_ssh_sg" {
  name        = "ssh_allow_only"
  vpc_id      = aws_vpc.testing_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name": "SSH AALOW Security Group"
  }
}

resource "aws_security_group" "allow_all_sg" {
  name        = "allow_all"
  vpc_id      = aws_vpc.testing_vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name": "SSH ALL Security Group"
  }
}

#Define web server isntances

resource "aws_instance" "outside_instances" {
  ami = "ami-0947d2ba12ee1ff75"
  count = 2
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.allow_ssh_sg.id]
  subnet_id = aws_subnet.public_testing_subnet.id
  key_name = "nupe"
  associate_public_ip_address = true
  tags = {
    "Name": "Outside_test_instance_${count.index+1}"
  }
}

resource "aws_instance" "inside_instances" {
  ami = "ami-0947d2ba12ee1ff75"
  instance_type = "t2.micro"
  count = 2
  vpc_security_group_ids = [aws_security_group.allow_ssh_sg.id]
  subnet_id = aws_subnet.private_testing_subnet.id
  key_name = "nupe"
  tags = {
    "Name": "Inside_test_instance_${count.index+1}"
  }
}

resource "aws_instance" "csr_test_router" {
  ami = "ami-039b2df9d192f211f"
  instance_type = "t2.medium"
  subnet_id = aws_subnet.public_testing_subnet.id
  vpc_security_group_ids = [aws_security_group.allow_ssh_sg.id]
  key_name = "nupe"
  private_ip = "10.10.0.21"
  tags = {
    "Name": "csr100v"
  }
}

resource aws_eip_association "router_int" {
  instance_id = aws_instance.csr_test_router.id
  allocation_id = "eipalloc-0e887aa171a8c3316"
}

resource "aws_network_interface" "private_csr_int" {
  subnet_id = aws_subnet.private_testing_subnet.id
  security_groups = [aws_security_group.allow_all_sg.id]
  private_ip = "10.10.10.21"
  attachment {
    device_index = 1
    instance = aws_instance.csr_test_router.id
  }
}

