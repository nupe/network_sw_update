variable "public_subnet" {
  type = string
  default = "10.10.0.0/24"
}

variable "private_subnet" {
  type = string
  default = "10.10.10.0/24"
}

variable "test_vpc_network" {
  type = string
  default = "10.10.0.0/16"
}

variable "AWS_ACCESS_KEY" {
  type = string
}

variable "AWS_SECRET_KEY" {
  type = string
}