#
# Code Testing and Validation VPC
#

resource "aws_vpc" "testing_vpc" {
  tags = {"Name" = "test_lab_vpc"}
  cidr_block = var.test_vpc_network
}

resource "aws_internet_gateway" "testing_vpc_igw" {
  vpc_id = aws_vpc.testing_vpc.id
}

resource "aws_subnet" "public_testing_subnet" {
  vpc_id = aws_vpc.testing_vpc.id
  cidr_block = var.public_subnet
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = "true"
}

resource "aws_subnet" "private_testing_subnet" {
  vpc_id = aws_vpc.testing_vpc.id
  cidr_block = var.private_subnet
  availability_zone = "us-east-1a"
}

resource "aws_route_table" "public_testing_subnet_rt" {
  vpc_id = aws_vpc.testing_vpc.id
}

resource "aws_route" "default" {
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.testing_vpc_igw.id
  route_table_id = aws_route_table.public_testing_subnet_rt.id
}

resource aws_route_table_association "public1" {
  route_table_id = aws_route_table.public_testing_subnet_rt.id
  subnet_id = aws_subnet.public_testing_subnet.id
}
