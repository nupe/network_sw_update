Gitlab CI/CD pipeline for automated testing and SW upgrade of CSR1000v running in an AWS environment


Pipeline overview below


To ensure a smooth software deployment for the CSR1000v in AWS pipeline accomplishes the following automated steps:

1. Certifying a new CSR1000v software version:
   - Building out test AWS test environment to include CSR1000V and four EC2 testing instances
   - Upgrading the Test environment with the new software version
   - Executing a series of test cases to confirm feature functionality
   - Test case results report generated and save as artifact at the conclusion of the test case execution 
   - If all test cases succeed, automatically move to Step #2 and destroy test environment

2. Implement new software version to AWS staging environment
   - First upgrade A-side CSR1000v
   - Once A-side SW update is complete, upgrade B-side CSR1000v
   - At this point staging CSR1000v should be upgraded with the new software version 

3. Pipeline should pause at this point
   - Opportunity to review testing results and allow new software version time to run in staging
   - Once pipeline is resumed by admin, move to step #4

4. Implement new software version in AWS Prod environment
   - First upgrade A-side CSR1000v
   - Once A-side SW update is complete, upgrade B-side CSR1000v
   - At this point Prod CSR1000v should be upgraded with the new software version 


Technologies utilized:
- Terraform for AWS Provisoning
- Ansible for SW upgrades and YAML file comparison
- Shell commands and scripting for CI/CD logic
- Python and pyATS framework for writing and executing test cases

