variable "staging_public_subnet" {
  type = string
  default = "10.11.0.0/24"
}

variable "staging_private_subnet" {
  type = string
  default = "10.11.10.0/24"
}

variable "prod_public_subnet" {
  type = string
  default = "10.12.0.0/24"
}

variable "prod_private_subnet" {
  type = string
  default = "10.12.10.0/24"
}

variable "staging_vpc_network" {
  type = string
  default = "10.11.0.0/16"
}

variable "prod_vpc_network" {
  type = string
  default = "10.12.0.0/16"
}

