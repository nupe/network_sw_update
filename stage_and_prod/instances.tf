# Define Instance security group
#
# Staging Instances

resource "aws_security_group" "staging_allow_ssh_sg" {
  name        = "staging_ssh_allow_only"
  vpc_id      = aws_vpc.staging_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name": "SSH AALOW Security Group"
  }
}

#Define staging CSR A

resource "aws_instance" "staging_csr_router_A" {
  ami = "ami-039b2df9d192f211f"
  instance_type = "t2.medium"
  subnet_id = aws_subnet.public_staging_subnet.id
  vpc_security_group_ids = [aws_security_group.staging_allow_ssh_sg.id]
  key_name = "nupe"
  private_ip = "10.11.0.21"
  tags = {
    "Name": "staging_csr100v_A"
  }
}

resource aws_eip_association "staging_router_int_A" {
  instance_id = aws_instance.staging_csr_router_A.id
  allocation_id = "eipalloc-0e887aa171a8c3316"
}

resource "aws_network_interface" "private_staging_csr_int_A" {
  subnet_id = aws_subnet.private_staging_subnet.id
  security_groups = [aws_security_group.staging_allow_ssh_sg.id]
  private_ip = "10.11.10.21"
  attachment {
    device_index = 1
    instance = aws_instance.staging_csr_router_A.id
  }
}


#Define staging CSR B

resource "aws_instance" "staging_csr_router_B" {
  ami = "ami-039b2df9d192f211f"
  instance_type = "t2.medium"
  subnet_id = aws_subnet.public_staging_subnet.id
  vpc_security_group_ids = [aws_security_group.staging_allow_ssh_sg.id]
  key_name = "nupe"
  private_ip = "10.11.0.22"
  tags = {
    "Name": "staging_csr100v_B"
  }
}

resource aws_eip_association "staging_router_int_B" {
  instance_id = aws_instance.staging_csr_router_B.id
  allocation_id = "eipalloc-011fa4ec83f745266"
}

resource "aws_network_interface" "private_csr_int_B" {
  subnet_id = aws_subnet.private_staging_subnet.id
  security_groups = [aws_security_group.staging_allow_ssh_sg.id]
  private_ip = "10.11.10.22"
  attachment {
    device_index = 1
    instance = aws_instance.staging_csr_router_B.id
  }
}


###########

#
# Prod Instances
#

resource "aws_security_group" "prod_allow_ssh_sg" {
  name        = "prod_ssh_allow_only"
  vpc_id      = aws_vpc.prod_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name": "SSH AALOW Security Group"
  }
}

#Define prod CSR A

resource "aws_instance" "prod_csr_router_A" {
  ami = "ami-039b2df9d192f211f"
  instance_type = "t2.medium"
  subnet_id = aws_subnet.public_prod_subnet.id
  vpc_security_group_ids = [aws_security_group.prod_allow_ssh_sg.id]
  key_name = "nupe"
  private_ip = "10.12.0.21"
  tags = {
    "Name": "prod_csr100v_A"
  }
}

resource aws_eip_association "prod_router_int_A" {
  instance_id = aws_instance.prod_csr_router_A.id
  allocation_id = "eipalloc-09b07e5dbe9b88d1f"
}

resource "aws_network_interface" "private_prod_csr_int_A" {
  subnet_id = aws_subnet.private_prod_subnet.id
  security_groups = [aws_security_group.prod_allow_ssh_sg.id]
  private_ip = "10.12.10.21"
  attachment {
    device_index = 1
    instance = aws_instance.prod_csr_router_A.id
  }
}


#Define staging CSR B

resource "aws_instance" "prod_csr_router_B" {
  ami = "ami-039b2df9d192f211f"
  instance_type = "t2.medium"
  subnet_id = aws_subnet.public_prod_subnet.id
  vpc_security_group_ids = [aws_security_group.prod_allow_ssh_sg.id]
  key_name = "nupe"
  private_ip = "10.12.0.22"
  tags = {
    "Name": "prod_csr100v_B"
  }
}

resource aws_eip_association "prod_router_int_B" {
  instance_id = aws_instance.prod_csr_router_B.id
  allocation_id = "eipalloc-08fc3eb4ce0568e6f"
}

resource "aws_network_interface" "private_prod_csr_int_B" {
  subnet_id = aws_subnet.private_prod_subnet.id
  security_groups = [aws_security_group.prod_allow_ssh_sg.id]
  private_ip = "10.12.10.22"
  attachment {
    device_index = 1
    instance = aws_instance.prod_csr_router_B.id
  }
}