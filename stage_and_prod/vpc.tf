#
# Code Testing and Validation VPC
#

resource "aws_vpc" "staging_vpc" {
  tags = {"Name" = "staging_vpc"}
  cidr_block = var.staging_vpc_network
}

resource "aws_internet_gateway" "staging_vpc_igw" {
  vpc_id = aws_vpc.staging_vpc.id
}

resource "aws_subnet" "public_staging_subnet" {
  vpc_id = aws_vpc.staging_vpc.id
  cidr_block = var.staging_public_subnet
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = "true"
}

resource "aws_subnet" "private_staging_subnet" {
  vpc_id = aws_vpc.staging_vpc.id
  cidr_block = var.staging_private_subnet
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = "true"
}


resource "aws_route_table" "public_staging_subnet_rt" {
  vpc_id = aws_vpc.staging_vpc.id
}

resource "aws_route" "staging_default" {
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.staging_vpc_igw.id
  route_table_id = aws_route_table.public_staging_subnet_rt.id
}

resource aws_route_table_association "staging_public" {
  route_table_id = aws_route_table.public_staging_subnet_rt.id
  subnet_id = aws_subnet.public_staging_subnet.id
}




#
# Prod definition
#

resource "aws_vpc" "prod_vpc" {
  tags = {"Name" = "prod_vpc"}
  cidr_block = var.prod_vpc_network
}

resource "aws_internet_gateway" "prod_vpc_igw" {
  vpc_id = aws_vpc.prod_vpc.id
}

resource "aws_subnet" "public_prod_subnet" {
  vpc_id = aws_vpc.prod_vpc.id
  cidr_block = var.prod_public_subnet
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = "true"
}

resource "aws_subnet" "private_prod_subnet" {
  vpc_id = aws_vpc.prod_vpc.id
  cidr_block = var.prod_private_subnet
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = "true"
}

resource "aws_route_table" "public_prod_subnet_rt" {
  vpc_id = aws_vpc.prod_vpc.id
}

resource "aws_route" "prod_default" {
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.prod_vpc_igw.id
  route_table_id = aws_route_table.public_prod_subnet_rt.id
}

resource aws_route_table_association "prod_public" {
  route_table_id = aws_route_table.public_prod_subnet_rt.id
  subnet_id = aws_subnet.public_prod_subnet.id
}



