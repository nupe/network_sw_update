from pyats import aetest
from genie.testbed import load
from genie.conf.base import Interface
import time
from genie.utils.diff import Diff


class CommonSetup(aetest.CommonSetup):

    #test case execute section

    @aetest.subsection
    def connect(self, testbed):
        #connect to devices
        testbed = load(testbed)
        devices = testbed.devices['dist-rtr01']
        devices.connect()
        self.parent.parameters.update(dev=devices)

class testcase01(aetest.Testcase):
    @aetest.test
    def test01(self, testbed):
        self.devices = self.parent.parameters['dev']
        sys.exit("stopping for now")
        self.ospf_pre = self.RT1.learn('ospf')

    @aetest.test
    def test02(self):
        #shutdown Loopback0
        loopback = Interface(device=self.RT1, name='Loopback0')
        loopback.shutdown = True
        print(loopback.build_config(apply=True))
        time.sleep(15)
    @aetest.test
    def test03(self):
        self.ospf_post = self.RT1.learn('ospf')



    @aetest.test
    def test04(self, steps):
        with steps.start(f"Testing if state to ensure no OSPF topology change occured", continue_=True) as step:
            diff = Diff(self.ospf_pre.info, self.ospf_post.info)
            diff.findDiff()
            print(diff)
            diff = str(diff)
            if 'spf_runs_count' in diff:
                step.failed()
            else:
                pass

class CommonCleanup(aetest.CommonCleanup):

    #Cleanup seciton
    pass