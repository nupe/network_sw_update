from pyats.easypy import run

### job file to kick off testing

def main(runtime):

    #run scripts
    run(testscript='/builds/nupe/network_sw_update/test_scripts/ospf_neighbors.py', taskid='OSPF neighbor test', runtime=runtime)
    run(testscript='/builds/nupe/network_sw_update/test_scripts/route_change.py', taskid='Route stability test', runtime=runtime)
    run(testscript='/builds/nupe/network_sw_update/test_scripts/eigrp_neighbors.py', taskid='OSPF neighbor route test', runtime=runtime)
    run(testscript='/builds/nupe/network_sw_update/test_scripts/interface_info.py', taskid='Interfaces test', runtime=runtime)
    run(testscript='/builds/nupe/network_sw_update/test_scripts/dmvpn.py', taskid='DMVPN test', runtime=runtime)
    run(testscript='/builds/nupe/network_sw_update/test_scripts/bgp_neighbors.py', taskid='BGP neighbor test', runtime=runtime)